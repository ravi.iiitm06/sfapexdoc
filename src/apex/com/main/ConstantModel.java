package apex.com.main;

import java.util.ArrayList;

/**
 * Apex constant information
 * 
 * @author Scott Covert
 */
public class ConstantModel extends PropertyModel {
  //---------------------------------------------------------------------------
  // Properties
  private String valueLine = "";

  //---------------------------------------------------------------------------
  // Methods
  public ConstantModel(String nameLine, ArrayList<String> comments, String valueLine) {
    super(nameLine.trim(), comments);
    this.valueLine = valueLine.trim();
  }
  
  public String getValue() {
    return valueLine;
  }
}