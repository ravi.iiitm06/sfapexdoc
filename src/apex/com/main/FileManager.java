package apex.com.main;

import java.io.*;
import java.util.*;

/**
 * Utility class for parsing text files and constructing the HTML
 * documentation files.
 *
 * @author Steve Cox
 */
public class FileManager {
  //---------------------------------------------------------------------------
  // Constants
  public static final String ROOT_DIRECTORY = "SfApexDocs";
  
  private static final String BODY_START = "<body>";
  private static final String BODY_END = "</body>";
  
  private static final String TOGGLE_ALL = "<span><input type='button' value='+/- all' onclick='ToggleAll(this);' /></span>";
  private static final String TOGGLE_ONE = "<input type='button' value='+'/>";
  
  
  //---------------------------------------------------------------------------
  // Properties
  protected String path;
  
  
  //---------------------------------------------------------------------------
  // Methods
  public FileManager() {
    this("");
  }
  
  public FileManager(String path) {
    SfApexDoc.assertPrecondition(null != path);
    
    this.path = path.trim();
    if (this.path.isEmpty()) this.path = ".";
  }
  
  public void createDocs(ArrayList<ClassModel> models, String detailFile, String homeFile) {
    SfApexDoc.assertPrecondition(null != models);
    SfApexDoc.assertPrecondition(null != detailFile);
    SfApexDoc.assertPrecondition(null != homeFile);
    
    Hashtable<String, String> classHashTable = new Hashtable<String, String>();
    
    String projectDetail = parseProjectDetail(detailFile.trim());
    if (projectDetail.isEmpty()) {
      projectDetail = HtmlConstants.DEFAULT_PROJECT_DETAIL;
    }
    
    String links = "<table width='100%'><tr>" + getPageLinks(models);
    
    String homeContents = parseHtmlFile(homeFile.trim());
    if (homeContents.isEmpty()) {
      homeContents = HtmlConstants.DEFAULT_HOME_CONTENTS;
    }
    
    if (!SfApexDoc.isEp) {
      homeContents = links + "<td><h2 class='section-title'>Home</h2>" + homeContents + "</td>";
      homeContents = HtmlConstants.HEADER_OPEN + projectDetail + HtmlConstants.HEADER_CLOSE +
            homeContents + HtmlConstants.FOOTER;
      classHashTable.put("index", homeContents);
      
      String all = "<table width='100%'><tr>";
      
      for (ClassModel model : models) {
        if (!model.getNameLine().isEmpty()) {
          String children = "";
          for (ClassModel child : model.children) {
            children += "<tr>" + createClassDoc(child, child.getName(), 3) + "</tr>";
          }
          
          final String fileName = model.getName();
          final String classDoc = createClassDoc(model, fileName, 1) + children + "</tr></table>";
          all += classDoc.replace(TOGGLE_ALL, "").replace(TOGGLE_ONE, "");
          
          classHashTable.put(fileName.toLowerCase(), HtmlConstants.HEADER_OPEN + projectDetail +
            HtmlConstants.HEADER_CLOSE + links + classDoc + HtmlConstants.FOOTER);
        }
      }
      
      classHashTable.put("_all", HtmlConstants.HEADER_OPEN.replace(HtmlConstants.HEADER_TOGGLE, "") +
        projectDetail + HtmlConstants.HEADER_CLOSE + all + HtmlConstants.FOOTER);
    } else { // EnablePath enhancement
      classHashTable.put("ep/index", HtmlConstants.EP_HEADER_OPEN + homeContents + HtmlConstants.EP_FOOTER);
      
      for (ClassModel model : models) {
        if (!model.getNameLine().isEmpty()) {
          String children = ""; // EnablePath enhancement
          for (ClassModel child : model.children) {
            children += createClassDoc(child, child.getName(), 3);
          }
          
          final String fileName = model.getName();
          classHashTable.put("ep/" + fileName.toLowerCase(),
            HtmlConstants.EP_HEADER_OPEN + createClassDoc(model, fileName, 1) + children +
            HtmlConstants.EP_FOOTER);
        }
      }
    }
    
    createDocFiles(classHashTable);
  }
  
  private String createClassDoc(ClassModel model, String fileName, Integer level) {
    model.addLinks();
    
    if (!SfApexDoc.isEp) {
      String contents = "<td class='classCell'>";
      contents +=
        "<h2 class='section-title'>" + fileName + TOGGLE_ALL + "</h2>" +
        "<div class='toggle_container_subtitle'>" + model.getNameLine() + "</div>" +
        "<table class='details' rules='all' border='1' cellpadding='6'>" +
        (model.getDescription().isEmpty() ? "" : "<tr><th>Description</th><td>" + model.getDescription() + "</td></tr>") +
        (model.getAuthor().isEmpty() ? "" : "<tr><th>Author</th><td>" + model.getAuthor() + "</td></tr>") +
        (model.getDate().isEmpty() ? "" : "<tr><th>Date</th><td>" + model.getDate() + "</td></tr>") +
        (model.getSee().isEmpty() ? "" : "<tr><th>See</th><td>" + model.getSee() + "</td></tr>") +
        "</table>";
      
      if (!model.properties.isEmpty()) {
        contents += "<p></p>" +
          "<h2 class='trigger'>" + TOGGLE_ONE + "&nbsp;&nbsp;<a href='#'>Properties</a></h2>" +
          "<div class='toggle_container'>" +
            "<table class='properties' border='1' rules='all' cellpadding='6'>";
        
        for (PropertyModel prop : model.properties) {
          String name = prop.getName();
          prop.addLinks();
          final String author = prop.getAuthor();
          final String date = prop.getDate();
          final String see = prop.getSee();
          contents += "<tr><th class='clsPropertyName'>" + name + "</th>" +
            "<td><div class='clsPropertyDeclaration'>" + prop.getNameLine() + "</div>" +
            "<div class='clsPropertyDescription'>" + prop.getDescription() +
              ((author.isEmpty() && date.isEmpty()) ? "" : " (" + author + " " + date + ")") +
               (see.isEmpty() ? "" : ", see " + see + prop.getThrowsAsString()) +
            "</div></tr>";
        }
        
        contents += "</table></div>";
      }

      if (!model.constants.isEmpty()) {
        if (model.properties.isEmpty()) {
          contents += "<p></p>";
        }
        contents += "<h2 class='trigger'>" + TOGGLE_ONE + "&nbsp;&nbsp;<a href='#'>Constants</a></h2>" +
          "<div class='toggle_container'>" +
            "<table class='constants' border='1' rules='all' cellpadding='6'>";
        
        for (ConstantModel constant : model.constants) {
          String name = constant.getName();
          constant.addLinks();
          final String author = constant.getAuthor();
          final String date = constant.getDate();
          final String see = constant.getSee();
          contents += "<tr><th class='clsConstantName'>" + name + "</th>" +
            "<td><div class='clsConstantDeclaration'>" + constant.getNameLine() + "</div>" +
            "<div class='clsConstantDescription'>" + constant.getDescription() +
              ((author.isEmpty() && date.isEmpty()) ? "" : " (" + author + " " + date + ")") +
               (see.isEmpty() ? "" : ", see " + see + constant.getThrowsAsString()) +
            "</div>" +
            "<div class='clsConstantValue'>" + constant.getValue() + 
            "</div></tr>";
        }
        
        contents += "</table></div>";
      }
      
      if (!model.methods.isEmpty()) {
        contents += "<h2 class='section-title methods'>Methods</h2>";
        for (MethodModel method : model.methods) {
          String name = method.getName();
          method.addLinks();
          contents += "<h2 class='trigger'>" + TOGGLE_ONE + "&nbsp;&nbsp;<a href='#'>" + name + "</a></h2>" +
            "<div class='toggle_container'>" +
            "<div class='toggle_container_subtitle'>" + method.getNameLine() + "</div>" +
            "<table class='details' rules='all' border='1' cellpadding='6'>" +
            (!method.getDescription().isEmpty() ? "<tr><th>Description</th><td>" + method.getDescription() + "</td></tr>" : "") +
            (!method.getAuthor().isEmpty() ? "<tr><th>Author</th><td>" + method.getAuthor() + "</td></tr>" : "") +
            (!method.getDate().isEmpty() ? "<tr><th>Date</th><td>" + method.getDate() + "</td></tr>" : "") +
            (!method.getReturns().isEmpty() ? "<tr><th>Returns</th><td>" + method.getReturns() + "</td></tr>" : "") +
            (method.getParams().size() > 0 ? "<tr><th colspan='2' class='paramHeader'>Parameters</th></tr>" : "");
          
          for (String param : method.getParams()) {
            if ((null != param) && !param.trim().isEmpty()) {
              if (param.indexOf(' ') != -1) {
                String list[] = param.split(" ");
                if (list.length >= 1) {
                  contents += "<tr><th class='param'>" + list[0] + "</th>";
                  String val = "";
                  if (list.length >= 2) {
                    for (int i = 1; i < list.length; i++) {
                      val += list[i] + " ";
                    }
                  }
                  contents = contents.trim() + "<td>" + val + "</td></tr>";
                }
              }
            }
          }
          
          contents += (method.getSee().isEmpty() ? "" : "<tr><th>See</th><td>" + method.getSee() + "</td></tr>");
          if (!method.getThrows().isEmpty()) {
            for (String t : method.getThrows()) {
              contents += "<tr><th>Throws</th><td>" + t + "</td></tr>";
            }
          }
          contents += "</table></div>";
        }
      }
      return contents + "</td>";
    } else { // EnablePath enhancement
      String contents = "<div class=\"class\"><h" + level + ">" + fileName + "</h" + level + ">" + model.getNameLine() + "</div>" +
        (model.getDescription().isEmpty() ? "" : "<div class=\"description\"><h" + (level+2) + ">Description</h" + (level+2) + ">" + model.getDescription() + "</div>") +
        (model.getAuthor().isEmpty() ? "" : "<div class=\"author\"><h" + (level+2) + ">Author</h" + (level+2) + ">" + model.getAuthor() + "</div>") +
        (model.getDate().isEmpty() ? "" : "<div class=\"date\"><h" + (level+2) + ">Date</h" + (level+2) + ">" + model.getDate() + "</div>") +
        (model.getSee().isEmpty() ? "" : "<div class=\"see\"><h" + (level+2) + ">See</h" + (level+2) + ">" + model.getSee() + "</div>");
      
      if (!model.properties.isEmpty()) {
        contents += "<div class=\"properties\"><h" + (level+1) + ">Properties</h" + (level+1) + ">";
        for (PropertyModel prop : model.properties) {
          String name = prop.getName();
          prop.addLinks();
          final String author = prop.getAuthor();
          final String date = prop.getDate();
          final String see = prop.getSee();
          contents += "<div class=\"property\">" +
            "<label>" + name + "</label>" +
            "<div class=\"signature\">" + prop.getNameLine() + "</div>" +
            "<div class=\"description\">" + prop.getDescription() +
              (author.isEmpty() && date.isEmpty()? "" : " (" + author + " " + date + ")") +
              (see.isEmpty() ? "" : ", see " + see) + prop.getThrowsAsString() + "</div>" +
            "</div>";
        }
        contents += "</div>";
      }
      
      if (!model.methods.isEmpty()) {
        contents += "<div class=\"methods\"><h" + (level+1) + ">Methods</h" + (level+1) + ">";
        for (MethodModel method : model.methods) {
          String name = method.getName();
          method.addLinks();
          contents += "<div class=\"method\">" +
            "<label>" + name + "</label>" +
            "<div class=\"signature\">" + method.getNameLine() + "</div>" +
            (!method.getDescription().isEmpty() ? "<div class=\"description\">" + method.getDescription() + "</div>" : "") +
            (!method.getAuthor().isEmpty() ? "<div class=\"author\">Author: " + method.getAuthor() + "</div>" : "") +
            (!method.getDate().isEmpty() ? "<div class=\"date\">Date: " + method.getDate() + "</div>" : "") +
            (!method.getReturns().isEmpty() ? "<div class=\"returns\">Returns: " + method.getReturns() + "</div>" : "");
          
          if (method.getParams().size() > 0) {
            contents += "<div class=\"params\"><h" + (level+3) + ">Parameters</h" + (level+3) + ">";
            for (String param : method.getParams()) {
              if ((null != param) && !param.trim().isEmpty()) {
                if (param.indexOf(' ') != -1) {
                  String list[] = param.split(" ");
                  if (list.length >= 1) {
                    contents += "<label>" + list[0] + "</label>";
                    String val = "";
                    if (list.length >= 2) {
                      for (int i = 1; i < list.length; i++) {
                        val += list[i] + " ";
                      }
                    }
                    contents = contents.trim() + val;
                  }
                }
              }
            }
            contents += "</div>";
          }
          
          contents += (method.getSee().isEmpty() ? "" : ("<div class=\"see\">See: " + method.getSee() + "</div>"));
          
          if (!method.getThrows().isEmpty()) {
            contents += "<div class=\"throws\">Throws: ";
            for (String t : method.getThrows()) {
              contents += t + " ";
            }
            contents = contents.trim() + "</div>";
          }
          
          contents += "</div>";
        }
        
        contents += "</div>";
      }
      
      return contents;
    }
  }
  
  private String parseProjectDetail(String filePath) {
    String contents = "";
    try {
      BufferedReader reader = new BufferedReader(new FileReader(filePath));
      String line;
      while ((line = reader.readLine()) != null) {
        line = line.trim();
        int equalsPos = line.indexOf("=");
        String key = (equalsPos >= 0) ? line.substring(0, equalsPos).trim() : "";
        String value = (equalsPos >= 0) ? line.substring(equalsPos + 1).trim() : "";
        if (key.equalsIgnoreCase("projectname")) {
          contents += "<h2>" + value + "</h2>";
        } else if (!value.isEmpty()) {
          contents += value + "<br/>";
        }
      }
      reader.close();
    } catch (Exception e) {
      SfApexDoc.log("parseProjectDetail(" + filePath + "): " + e.getMessage());
    }
      
    return contents.trim();
  }
  
  private String parseHtmlFile(String filePath) {
    String contents = "";
    try {
      BufferedReader reader = new BufferedReader(new FileReader(filePath));
      String line;
      while ((line = reader.readLine()) != null) {
        contents += line.trim();
      }
      reader.close();
    } catch (Exception e) {
      SfApexDoc.log("parseHtmlFile(" + filePath + "): " + e.getMessage());
    }
    
    int bodyStart = contents.indexOf(BODY_START);
    if (bodyStart >= 0) {
      int bodyEnd = contents.indexOf(BODY_END);
      if (bodyEnd >= 0) {
        contents = contents.substring(bodyStart + BODY_START.length(), bodyEnd);
      }
    }
    
    return contents.trim();
  }
  
  
  //---------------------------------------------------------------------------
  // Helpers
  private void copyFile(String source, String target) throws Exception {
    target += "/" + source;
    if (!(new File(target).exists())) {
      InputStream is = getClass().getResourceAsStream(source);
      FileOutputStream to = new FileOutputStream(target);
      byte[] buffer = new byte[4096];
      int bytesRead;
      while ((bytesRead = is.read(buffer)) >= 0) {
        to.write(buffer, 0, bytesRead);
      }
      to.flush();
      to.close();
      is.close();
    }
  }
  
  private void createDocFiles(Hashtable<String, String> classHashTable) {
    try {
      // create required folders for documentation files
      if (!path.endsWith("/") && !path.endsWith("\\")) {
        path += '/';
      }
      path += ROOT_DIRECTORY;
      //(new File(path)).mkdirs();
      (new File(path + "/ep")).mkdirs(); // EnablePath enhancement
      
      for (String fileName : classHashTable.keySet()) {
        SfApexDoc.log("Processing: " + fileName);
        
        File file= new File(path + "/" + fileName + ".html");
        FileOutputStream fos = new FileOutputStream(file);
        DataOutputStream dos = new DataOutputStream(fos);
        dos.writeBytes(classHashTable.get(fileName));
        dos.close();
        fos.close();
        
        SfApexDoc.instance.showProgress();
      }
      
      copyResources(path);
    } catch(Exception e) {
      SfApexDoc.log(e);
    }
  }
  
  private String getPageLinks(ArrayList<ClassModel> models){
    // get a sorted list of the class names
    ArrayList<String> classNames = new ArrayList<String>();
    for (ClassModel model : models) {
      String name = model.getName();
      if (!name.isEmpty()) {
        classNames.add(name);
      }
    }
    Collections.sort(classNames);
    
    // build the html links
    String links = "<td class='leftmenus' rowspan='100'><div onclick=\"gotomenu('index.html');\">Home</div>";
    for (String className : classNames) {
      links += "<div onclick=\"gotomenu('" + className.toLowerCase() + ".html');\">" + className + "</div>";
    }
    return links + "</td>";
  }
  
  private void copyResources(String toPath) throws IOException, Exception {
    copyFile("logo.png", toPath);
    copyFile("SfApexDoc.css", toPath);
    copyFile("h2_trigger_a.gif", toPath);
    copyFile("jquery-latest.js", toPath);
    copyFile("toggle_block_btm.gif", toPath);
    copyFile("toggle_block_stretch.gif", toPath);
  }
}