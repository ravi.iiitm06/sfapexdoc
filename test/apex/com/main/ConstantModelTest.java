package apex.com.main;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;

public class ConstantModelTest {
  @Test
  public void testGetValue() {
    String name = "Name";
    ArrayList<String> comments = new ArrayList<String>();
    
    ConstantModel c = new ConstantModel(name, comments, "");
    assertEquals("", c.getValue());
    
    c = new ConstantModel(name, comments, " ");
    assertEquals("", c.getValue());
    
    c = new ConstantModel(name, comments, "\t");
    assertEquals("", c.getValue());
    
    c = new ConstantModel(name, comments, "value");
    assertEquals("value", m.getValue());
    
    c = new ConstantModel(name, comments, " value");
    assertEquals("value", m.getName());
    
    c = new ConstantModel(name, comments, "value ");
    assertEquals("value", m.getName());
    
    c = new ConstantModel(name, comments, " value ");
    assertEquals("value", m.getName());    
  }
}